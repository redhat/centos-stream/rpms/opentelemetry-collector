%global goipath         github.com/os-observability/redhat-opentelemetry-collector

Version:                0.107.0
ExcludeArch:            %{ix86} s390 ppc ppc64

%gometa

%global common_description %{expand:
Collector with the supported components for a Red Hat build of OpenTelemetry}

%global golicenses    LICENSE
%global godocs        README.md

Name:           opentelemetry-collector
Release:        5%{?dist}
Summary:        Red Hat build of OpenTelemetry

License:        Apache-2.0

Source0:        redhat-%{name}-%{version}.tar.gz
Source1:        otel_collector_journald.te

BuildRequires: systemd
BuildRequires: %{?go_compiler:compiler(go-compiler)}%{!?go_compiler:golang}
BuildRequires: binutils
BuildRequires: git
BuildRequires: policycoreutils, checkpolicy, selinux-policy-devel

Requires(pre): shadow-utils
Requires(pre): util-linux
Requires(pre): /usr/sbin/useradd, /usr/bin/getent
Requires(postun): /usr/sbin/userdel

%description
%{common_description}

%prep
mkdir -p _build
mkdir -p _build/bin

%setup -q -n redhat-%{name}-%{version}

%build

# Compile the SELinux policy module
checkmodule -M -m -o otel_collector_journald.mod %{SOURCE1}
semodule_package -o otel_collector_journald.pp -m otel_collector_journald.mod

go build -ldflags "-s -w" -v -buildmode pie -mod vendor -o %{gobuilddir}/bin/opentelemetry-collector

%define debug_package %{nil}

%install
# create expected directory layout
mkdir -p %{buildroot}%{_datadir}/selinux/packages
mkdir -p %{buildroot}%{_sysconfdir}/opentelemetry-collector
mkdir -p %{buildroot}%{_sysconfdir}/opentelemetry-collector/configs
mkdir -p %{buildroot}%{_unitdir}

# install files
install -m 0644 ./otel_collector_journald.pp %{buildroot}%{_datadir}/selinux/packages/otel_collector_journald.pp
install -p -m 0644  ./00-default-receivers.yaml %{buildroot}%{_sysconfdir}/opentelemetry-collector/configs/00-default-receivers.yaml
install -p -m 0644  ./opentelemetry-collector.service %{buildroot}%{_unitdir}/%{name}.service

install -m 0755 -vd                     %{buildroot}%{_bindir}
install -m 0755 -vp %{gobuilddir}/bin/* %{buildroot}%{_bindir}/
install -m 0755 -p ./opentelemetry-collector-with-options %{buildroot}%{_bindir}/

%pre
/usr/bin/getent group observability > /dev/null || /usr/sbin/groupadd -r observability
/usr/bin/getent passwd observability > /dev/null || /usr/sbin/useradd -r -M -s /sbin/nologin -g observability -G systemd-journal observability

%postun
if [ $1 -eq 0 ]; then
    /usr/sbin/userdel observability
fi

%post
semodule -i %{_datadir}/selinux/packages/otel_collector_journald.pp
restorecon -v %{_bindir}/opentelemetry-collector
/bin/systemctl --system daemon-reload 2>&1

%preun
if [ $1 -eq 0 ]; then
    /bin/systemctl --quiet stop %{name}.service
    /bin/systemctl --quiet disable %{name}.service
    semodule -r otel_collector_journald
fi

%posttrans
/bin/systemctl is-enabled %{name}.service >/dev/null 2>&1
if [  $? -eq 0 ]; then
    /bin/systemctl restart %{name}.service >/dev/null
fi

%check
%gocheck

%files
%{_unitdir}/%{name}.service
%{_sysconfdir}/opentelemetry-collector/configs/00-default-receivers.yaml
%{_datadir}/selinux/packages/otel_collector_journald.pp

%license %{golicenses}
%doc %{godocs}
%{_bindir}/*

%changelog
* Mon Mar 03 2025 Conor Cowman <ccowman@redhat.com> - 0.107.0-5
- Bump revision
- Add runtime requirements for shadow-utils and util-linux to ensure successful creation of observability user on installation
- Modify pre-uninstallation stage to only delete delete the observability user on full uninstallation to prevent the user being deleted during upgrades
  Resolves: RHEL-76915

* Wed Feb 12 2025 Conor Cowman <ccowman@redhat.com> - 0.107.0-4
- Bump revision
- Fix reversion of previous dependency updates post golang update
  Resolves: RHEL-79109

* Tue Feb 11 2025 Conor Cowman <ccowman@redhat.com> - 0.107.0-3
- Bump revision
- Update tarball golang from 1.21.0 to 1.22.11
- Update addresses CVE-2024-45336
  Resolves: RHEL-78944

* Fri Feb 07 2025 Conor Cowman <ccowman@redhat.com> - 0.107.0-2
- Bump revision
- Update tarball name to match upstream 
- Upgrade the following tarball dependencies:
- x/sys v0.23.0 to v0.29.0
- x/crypto v0.26.0 to v0.32.0
- x/net v0.28.0 to v0.33.0
- x/sync v0.8.0 to v0.10.0
- x/term v0.23.0 to v0.28.0
- x/text v0.17.0 to v0.21.0
- Update addresses the following CVEs: 
- CVE-2024-45338
- CVE-2024-45337
  Resolves: RHEL-78346

* Thu Nov 28 2024 Felix Kolwa <fkolwa@redhat.com> - 0.107.0-1
- Version bump to 0.107.0
- Reset release to 1
- Update addresses the following CVEs:
- CVE-2024-34155
- CVE-2024-34156
- CVE-2024-42368

* Tue Oct 29 2024 Troy Dawson <tdawson@redhat.com> - 0.102.1-6
- Bump release for October 2024 mass rebuild:
  Resolves: RHEL-64018

* Mon Sep 09 2024 Major Hayden <major@redhat.com> - 0.102.1-5
- Rebuilt for C10S/RHEL 10

* Mon Aug 19 2024 Felix Kolwa <fkolwa@redhat.com> - 0.102.1-4
- include aarch64 build target
* Thu Aug 01 2024 Benedikt Bongartz <bongartz@redhat.com> - 0.102.1-3
- Add default selinux policy for journald receiver
- Bump revision
* Wed Jul 24 2024 Benedikt Bongartz <bongartz@redhat.com> - 0.102.1-2
- spec: strip go binary
* Tue Jul 16 2024 Benedikt Bongartz <bongartz@redhat.com> - 0.102.1-1
- rpm: trim date (#89) (Ben B)
- Add transform processor (#88) (Ruben Vargas)
* Fri Jun 28 2024 Benedikt Bongartz <bongartz@redhat.com> - 0.102.1
- move microshift specifics into another rpm
- bump collector version to 0.102.0
* Fri Apr 12 2024 Benedikt Bongartz <bongartz@redhat.com> - 0.95.0
- add observability user that is part of the systemd-journal group
- add opentelemetry collector config folder (`/etc/opentelemetry-collector/configs`)
- add opentelemetry collector default config
- add microshift manifests
* Thu Feb 1 21:59:10 CET 2024 Nina Olear <nolear@redhat.com> - 0.93.4
- First package for Copr
